std::vector<cv::String> getOutputsNames(const cv::dnn::Net& net)
{
	static std::vector<cv::String> names;
	if(names.empty())
	{
		//Get the indiices of the output layers,i .e. the layers with unconnected outputs
		std::vector<int> outLayers = net.getUnconnectedOutLayers();

		//get the names of all the layers in the network
		std::vector<cv::String> layerNames = net.getLayerNames();
		

		//get the names of the output layers in names
		names.resize(outLayers.size());
		for(size_t i = 0 ;i < outLayers.size();i++)
		{
			names[i] = layerNames[outLayers[i] - 1];
		}
	}
	return names;
}

/*
*
* @descritpion:std::asyncis a function that creates and runs a new asynchronous task, it takes two arguments:
* -std::launch::asynch- this specifies the task should be launched asynchorously, meanint it will start running immediatly in a seperate thread
* -[](){.....} - this is a lambda function that defines the task to be executed asynchronously
*  in the return std::system(...) within theeee lambda functioon passsssssed to the std::asynch ,  std:;systeeeem is caalled to execute
* a command in the shell. The command to be executed is :"sudo nice -n -20 libcamera-vid -n -t 0 --rotation 180 --width 680  --height 692 --framerate 30 --inline --listen -o tcp://127.0.0.1:8888"
*
* @return : The std::future<int> returned by std::async represents the result of the asynchronous task. In this case it represents the exit status of the command executed by std::system.
*/

std::future<int> startProgramAsync()
{
    return std::async(std::launch::async,[]()
    {
        return std::system("sudo nice -n -20 libcamera-vid -n -t 0 --rotation 180 --width 680  --height 692 --framerate 30 --inline --listen -o tcp://127.0.0.1:8888");

    });
}

/*
* @descritpion:
*
* @return:
*/
void checkAsyncResult(std::future<int>& result)
{
    if(result.valid())
	{
		std::cout << "Command started asynchronously " <<std::endl;
	}
	else
	{
		std::cout << "failed to start the command asynchronously " << std::endl;
		exit(0);
	}
}

/*
* @descritpion:
*
* @return:
*/
cv::VideoCapture establishConnection(const std::string &streamAddress)
{
    cv::VideoCapture cap(streamAddress);
    if(!cap.isOpened())
    {
        std::cout << "Could not open the stream service, pleace check the address" << std::endl;
        std::cout << "Exiting programm !" << std::endl;
        exit(0);
    }
    else
    {
        std::cout << "Stream service is active, the address is correct" << std::endl;
    }
    return cap;
}