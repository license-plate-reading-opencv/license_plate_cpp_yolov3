#include <iostream>
#include <stdlib.h>
#include <opencv2/opencv.hpp>
#include <future>
#include <chrono>
#include <thread>



std::vector<cv::String> getOutputsNames(const cv::dnn::Net& net);

//////////////////start asynchronos programms/////////////////////////
std::future<int> startProgramAsync();
void checkAsyncResult(std::future<int>& result);

//////////////////start video capture using network streaming for raspberry pi Camera V2/////////////////////////
cv::VideoCapture establishConnection(const std::string &streamAddress);

