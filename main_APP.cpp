#include "main.hpp"
#include "licens_det_obj.hpp"



int main(void)
{
	cv::Mat frame, blob;
	cv::Point classIdPoint;
	double confidence;
	int centerX,centerY,width,height,left,top;
	//start the programm asynchronous 
	std::future<int> result = startProgramAsync();
	//Check if the command was successful
	checkAsyncResult(result);
	
	//Wait for some time for the program to start
	std::this_thread::sleep_for(std::chrono::seconds(3));
	
	//Establish the TCP connection(client) with the stream (server)
	std::string streamAddress = "tcp://127.0.0.1:8888"; 
	cv::VideoCapture cap = establishConnection(streamAddress);

	//Paths for the YOLO configuration file and weights file 
	std::string pathYoloCfg = "./yolov3_model/cfg/darknet-yolov3.cfg";
	std::string pathYoloWeights = "./yolov3_model/weights/model.weights";
	
	//Read a network model sored in the Darknet model file
	//the neural netwrok model arhitecture is stored in the configuration file
	//the pre-trained weights of the neural network are store in the wieghts file
	cv::dnn::Net license = cv::dnn::readNetFromDarknet(pathYoloCfg,pathYoloWeights);
	license.setPreferableTarget(cv::dnn::DNN_TARGET_CPU);
	license.setPreferableBackend(cv::dnn::DNN_BACKEND_OPENCV);
	
	
	
	std::vector<cv::String>layersNames = license.getLayerNames();
	std::vector<int> outLayers = license.getUnconnectedOutLayers();
	
	while(true)
	{	
		//Capture frame-by-frame to the header 
		cap >> frame;
		//check if the frame is empty
		if(frame.empty())
		{
			std::cout << "Error: Failed to capture frame"<<std::endl;
			exit(0);
		}
		cv::dnn::blobFromImage(frame,blob,1/255.0,cv::Size(416,416),cv::Scalar(255,0,255),true,false);	
		//Sets the input to the network
		license.setInput(blob);
		std::vector<cv::Mat>outs;
	
		license.forward(outs,layersNames);
		std::vector<cv::Rect> plates;
		//post-process the detection
		for(auto& out: outs)
		{
			float* data = (float*)out.data;
			for(int j = 0 ; j < out.rows;++j,data+=out.cols)
			{
				cv::Mat score = out.row(j).colRange(5,out.cols);
				cv::minMaxLoc(score,0,&confidence,0,&classIdPoint);
				if(confidence  > 0.2)
				{
					centerX = (int)(data[0] * frame.cols);
					centerY = (int)(data[1] * frame.rows);
					width = (int)(data[2] * frame.cols);
					height = (int)(data[3] * frame.rows);
					left = centerX - width /2;
					top = centerY - height/2;
					plates.push_back(cv::Rect(left,top,width,height));
				}
			}
		}
		for(size_t i = 0 ; i < plates.size();i++)
		{
			cv::rectangle(frame,plates[i],cv::Scalar(255,0,255),3);
		}
		//Display the frame
		cv::imshow("Frame",frame);
		//check for 'q' key press to exit
		if(cv::waitKey(1) == 'q')
		{
			break;
		}

	}

	//Release the video capture object
	cap.release();
	//Destroy all OpenCV windows
	cv::destroyAllWindows();

	return 0;
}
